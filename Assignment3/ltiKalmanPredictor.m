function data_prediction = ltiKalmanPredictor(noisy_pos, x0, P0, A, C, Q, R, Kinf)
    len_pos = length(noisy_pos);

    for i = 1:len_pos
        % position
        data_prediction(i).pos = noisy_pos(i);
        if i == 1
            data_prediction(i).xkp1_k = x0;
        else
            prev_point = data_prediction(i-1);
            x_prev = prev_point.xkp1_k;
            data_prediction(i).xkp1_k = A*x_prev+A*Kinf*(data_prediction(i).pos-C*x_prev);
        end
    end
end


