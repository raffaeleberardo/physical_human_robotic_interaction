close all;

% Time-series
Ts = 0.001;

noisy_pos = out.position_GaussianNoise.signals.values;
t = out.position_GaussianNoise.time;

k = 0.01;
R = k*Ts; % noise variance

% velocity: System as speed estimation - kinematic model
A = [1 Ts
     0 1];
B = [Ts^2/2; Ts];
Q = k*B*B';
C = [1 0];

% initial state
x0 = [0;0];
% initial covariance matrix, small numbers 
P0 = eye(2)*0.1;

% filtering
data = kalmanFilter(noisy_pos, x0, P0, A, C, Q, R);
% prediction
data_prediction = kalmanPredictor(noisy_pos, x0, P0, A, C, Q, R);

% smoothing
data = kalmanSmoother(data, A);


% velocity as Euler approximation
estimated_vel = (noisy_pos(2:end)-noisy_pos(1:end-1))/Ts;
estimated_vel = [0;estimated_vel];
% Measured velocity
vel = out.velocity.signals.values; 

estimate_f_v = [data.xk_k]; %filter
estimate_s_v = [data.xk_N]; % smoothing
estimate_p_v = [data_prediction.xkp1_k]; % prediction

% PLOT

figure
plot(t, lowPassFilter(estimated_vel,1,Ts),"Color",'r');
hold on
plot(t,vel,"LineWidth",3, 'Color','b')
plot(t,estimate_f_v(2,:),"LineWidth",3,'Color','g')
plot(t,estimate_p_v(2,:), "LineWidth",3,"Color",'m')
plot(t,estimate_s_v(2,:),"LineWidth",3,"Color",'k')
title('Velocity')
legend('Euler Approximated Velocity', 'Measured Velocity', 'Filtered Velocity', 'Predicted Velocity', 'Smooth Velocity')
hold off

% LTI filter - velocity

Pinf = idare(A', C', Q, R); % discrete time Algebraic Riccati Equation
Kinf = Pinf*C'*inv(C*Pinf*C'+R);

data = ltiKalmanFilter(noisy_pos, x0, P0, A, C, Q, R, Kinf);
estimate_f_v = [data.xk_k];

figure
plot(t, lowPassFilter(estimated_vel,1,Ts),"Color",'r');
hold on
plot(t,vel,"LineWidth",3, 'Color','b')
plot(t, estimate_f_v(2,:), '-.', "LineWidth",3,'Color','g');
% LTI predictor - velocity
data_prediction = ltiKalmanPredictor(noisy_pos, x0, P0, A, C, Q, R, Kinf);
estimate_p_v = [data_prediction.xkp1_k];
plot(t, estimate_p_v(2,:), '-.', "LineWidth",3,'Color','m');
title('Velocity - LTI')
legend('Euler Approximated Velocity', 'Measured Velocity', 'LTI kalman filter', 'LTI kalman predictor')
hold off

% acceleration
A = [1 Ts Ts^2/2
     0  1   Ts
     0  0   1];
B = [Ts^3/6;Ts^2/2;Ts];
Q = k*B*B';
C = [1 0 0];

x0 = [0;0;0];
P0 = eye(3)*0.1;

data = kalmanFilter(noisy_pos, x0, P0, A, C, Q, R);

% Smoothing
data = kalmanSmoother(data, A);

data_prediction = kalmanPredictor(noisy_pos, x0, P0, A, C, Q, R);

estimate_f_a = [data.xk_k];
estimate_s_a = [data.xk_N];
estimate_p_a = [data_prediction.xkp1_k];

acc = out.acceleration.signals.values;

figure
plot(t,acc,"LineWidth",2,"Color",'r')
hold on
plot(t,estimate_f_a(3,:),"LineWidth",3,"Color",'g')
plot(t,estimate_p_a(3,:),"LineWidth",3, "Color",'m')
plot(t,estimate_s_a(3,:),"LineWidth",3, "Color",'k');
title('Acceleration')
legend('Measured Acceleration', 'Filtered Acceleration', 'Predicted Acceleration', 'Smooth Acceleration')
hold off

% LTI filter - acceleration
Pinf = idare(A', C', Q, R); % discrete time Algebraic Riccati Equation
Kinf = Pinf*C'*inv(C*Pinf*C'+R);

data = ltiKalmanFilter(noisy_pos, x0, P0, A, C, Q, R, Kinf);
estimate_f_a = [data.xk_k];
figure
plot(t,acc,"LineWidth",3,"Color",'r');
hold on
plot(t, estimate_f_a(3,:), '-.', "LineWidth",3,"Color",'g');

% LTI predictor - acceleration
data_prediction = ltiKalmanPredictor(noisy_pos, x0, P0, A, C, Q, R, Kinf);
estimate_p_a = [data_prediction.xkp1_k];
plot(t, estimate_p_a(3,:), '-.', "LineWidth",3,"Color",'m');
title('Acceleration - LTI')
legend('Measured Acceleration', 'LTI kalman filter', 'LTI kalman predictor')
hold off
