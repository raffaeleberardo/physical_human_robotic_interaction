function data_prediction = kalmanPredictor(noisy_pos, x0, p0, A, C, Q, R)
    len_pos = length(noisy_pos);

    for i = 1:len_pos
        % position
        data_prediction(i).pos = noisy_pos(i);
        if i == 1
            % first step assign the initial states
            data_prediction(i).xkp1_k = x0;
            data_prediction(i).Pkp1_k = p0;
        else
            prev_point = data_prediction(i-1);
            P_prev = prev_point.Pkp1_k;
            x_prev = prev_point.xkp1_k;
            K = A*P_prev*C'*inv(C*P_prev*C'+R);
            data_prediction(i).xkp1_k = A*x_prev+K*(data_prediction(i).pos-C*x_prev);
            data_prediction(i).Pkp1_k = A*P_prev*A'-K*C*P_prev*A' + Q;
        end
    end
end

