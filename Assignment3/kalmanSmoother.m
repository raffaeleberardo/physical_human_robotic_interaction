function data = kalmanSmoother(data, A)
    % in the kalman smoother we implement just the backwatd step
    N = length(data);
    for i=N:-1:1
        if i == N
            data(i).xk_N = data(i).xk_k;
            data(i).Pk_N = data(i).Pk_k;
        else
            K = data(i).Pk_k*A'*inv(data(i).Pkp1_k);
            data(i).xk_N = data(i).xk_k+K*(data(i+1).xk_N-data(i).xkp1_k);
            data(i).Pk_N = data(i).Pk_k+K*(data(i+1).Pk_N-data(i).Pkp1_k);
        end
    end
end
