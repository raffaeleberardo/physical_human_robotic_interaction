function data = kalmanFilter(noisy_pos, x0, P0, A, C, Q, R)

    len_pos = length(noisy_pos);

    for i = 1:len_pos
        % position
        data(i).pos = noisy_pos(i);
        if i == 1
            % First values are the initial states
            data(i).xk_k = x0;
            data(i).Pk_k = P0;
        else
            prev_point = data(i-1);
            p_prev = prev_point.Pkp1_k;
            % Kalman Gain
            K = p_prev*C'*inv(C*p_prev*C'+R);
            % Estimation Step
            data(i).xk_k = prev_point.xkp1_k+K*(data(i).pos-C*prev_point.xkp1_k);
            data(i).Pk_k = p_prev-K*C*p_prev;
        end
        % Prediction Step
        data(i).xkp1_k = A*data(i).xk_k;
        data(i).Pkp1_k = A*data(i).Pk_k*A' + Q;
end

