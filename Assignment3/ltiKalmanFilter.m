function data = ltiKalmanFilter(noisy_pos, x0, P0, A, C, Q, R, Kinf)
    len_pos = length(noisy_pos);

    for i = 1:len_pos
        % position
        data(i).pos = noisy_pos(i);
        if i == 1
            data(i).xk_k = x0;
        else
            prev_point = data(i-1);
            data(i).xk_k = A*prev_point.xk_k+Kinf*(data(i).pos-C*A*prev_point.xk_k);
        end
    end
end

