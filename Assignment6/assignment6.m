% Sample parameters for four channel bilateral teleoperation
clear all;
close all;

% Time series
Ts = 0.001;

% Input function parameter (sin or step with low pass filter)
A_int = 6;

% Low pass frequency cuff off
Flp = 0.5;
% Sin frequency
Fc = 1; 

% Human intention controller (PD)

% Position-position
Ph = 80*1; 
Dh = 20*0.8; 

% Force-position
% Ph = 10*1; 
% Dh = 20*0.8; 

% Human impedance parameters
Jh = 1;
Bh = 1; 

% Inertia/Damping of robot dynamics
Mm = 0.5;
Ms = 2;

% Make the real pole with re<0 to change the inertia of the robot
% dynamics verify this value by adding them to the simulink model
% Dm = 5;
% Ds = 10;
Dm = 0;
Ds = 0;

% Master controller
Bm = 20*0.8;
Km = 10*1;

% Slave controller
Bs = 4*Bm; 
Ks = 4*Km; 

% Environment impedance parameters
Be = 0; 
Ke = 10; 
xe = 5;

% Delay var (some delay bring instability even with no touch with env)
delay = 20;

% Kalman filter parameters
A = [1 Ts
    0 1];
B = [Ts^2/2;Ts];
x0 = [0 0];
C = [1 0];

q_m = 1000000000;
Q_m = q_m*B*B';
q_s = 1000000000;
Q_s = q_s*B*B';
R = 1;
noiseVariance = 0;
% noiseVariance = 0.00001;

%Two-layer architecture parameters
alpha = 0.1; %needed for the TLC
beta = 0.1;
Hd = 30;
H0_master = 0;
H0_slave = 0;