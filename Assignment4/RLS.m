function pred_y_rls = RLS(X,Y,lambda)

    p0 = eye(2)*0.01;
    b0 = [0;0];
    if lambda > 1
        lambda = 1;
    elseif lambda <= 0
        lambda = 0.005;
    end

    x = X(1,:);
    y = Y(1);
    e = y-x*b0;
    p = (p0-(p0*x'*x*p0)/(lambda+x*p0*x'))/lambda;
    k = p*x';
    b = b0+k*e;
    pred_y_rls(1)=x*b;
    for i=2:length(Y)
        x = X(i,:);
        y = Y(i);
        e = y - x*b;
        p = (p-(p*x'*x*p)/(lambda+x*p*x'))/lambda;
        k = p*x';
        b = b + k*e;
        pred_y_rls(i)=x*b;
    end
end

