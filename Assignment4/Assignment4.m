addpath KalmanFilters;

close all;

% Voltages
m_volt = out.voltages.Data;
t = out.voltages.Time;

% Noisy position
m_pos = out.positions.Data;

% Real velocity
real_vel = out.real_vel.Data;

%% Kalman smoother filter - VELOCITY
q = 0.01;
R = q*Ts;

A = [1 Ts
     0 1];
B = [Ts^2/2; Ts];
Q = q*B*B';
C = [1 0];
x0 = [0;0];
P0 = eye(2)*0.1;

data = kalmanFilter(m_pos, x0, P0, A, C, Q, R);
data = kalmanSmoother(data, A);

% Calculated velocity from noisy position with kalman smoother
kalmanVel = [data.xk_N];

% plot velocities
figure
plot(t,real_vel,'LineWidth',3, "Color",'r')
hold on
plot(t,kalmanVel(2,:),'LineWidth',3,'Color','k')
title('Velocity')
legend('Real', 'Kalman smoother')
hold off

%% Kalman smoother filter - ACCELERATION
A = [1 Ts Ts^2/2
     0  1   Ts
     0  0   1];
B = [Ts^3/6;Ts^2/2;Ts];
Q = q*B*B';
C = [1 0 0];

x0 = [0;0;0];
P0 = eye(3)*0.1;

data = kalmanFilter(m_pos, x0, P0, A, C, Q, R);
data = kalmanSmoother(data, A);

% Calculated acceleration from noisy position with kalman smoother
kalmanAcc = [data.xk_N]; 

% plot accelerations
figure
plot(t,kalmanAcc(3,:),'LineWidth',3, 'Color','r')
title('Acceleration')
legend('Kalman smoother')
hold off

%% LS
X = [kalmanAcc(3,:)',kalmanVel(2,:)']; % X=[W_dot, W]
Y = m_volt;

beta = inv(X'*X)*X'*Y;
k_LS = 1/beta(2)
tau_LS = k_LS*beta(1)
pred_y = X*beta;

% RLS
% This parameter defines how fast the RLS algorithm should forget past 
% data samples. If we set it to 1 the algorithm uses all available data 
% samples to estimate the filter weights. As we decrease this value the 
% contribution of past samples to the weight estimation decreases.
lambda = 0.4;

% betaRLS is actually useless as it only the refers to the last iteration
% so k_RLS and tau_RLS
pred_y_rls = RLS(X,Y,lambda);

% plot LS - RLS
figure
plot(t,Y, 'LineWidth',4, 'Color',"#A2142F")
hold on
plot(t,pred_y,'LineWidth',3,'Color',"#77AC30")
plot(t,pred_y_rls,'-.','LineWidth',2, 'Color',"#4DBEEE")
title('Voltages')
legend('Voltage', 'Least Squares', 'Recursive Least Squares')
xlabel('Time')
ylabel('Voltage')
hold off
